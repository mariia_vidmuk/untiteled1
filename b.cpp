#include <iostream>
#include "string"
using namespace std;
struct Node{
    int val;
    Node *LeftNode;
    Node *RightNode;
};
void Add(int new_val, Node *&new_Node){
    if(!new_Node){
        new_Node = new Node;
        new_Node->val = new_val;
        new_Node->LeftNode = nullptr;
        new_Node->LeftNode = nullptr;
    }
    else{
        if(new_Node->val > new_val){
            Add(new_val, new_Node->LeftNode);
        }
        else{
            Add(new_val, new_Node->RightNode);
        }
    }
}
void Show(Node *node)
{
    if (!node)
        return;
    Show(node->LeftNode);
    cout << node->val<< endl;
    Show(node->RightNode);
}

int main(){

    Node *node = nullptr;
    int x;
    for (int i = 0; i < 10; ++i){
        x = rand()%10 + 1;
        Add(x, node);
    }
    return 0;
}
